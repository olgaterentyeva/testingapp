package com.company;

import java.util.ArrayList;

public class Questions {

    private String question;
    private ArrayList<String> answers;
    private String rightAnswer;
    private int amountOfPoints;

    public String getQuestion() {
        return question;
    }
    public ArrayList<String> getAnswers() {
        return answers;
    }
    public String getRightAnswer() {
        return rightAnswer;
    }
    public int getAmountOfPoints() {
        return amountOfPoints;
    }

    public Questions(String question, ArrayList<String> answers, String rightAnswer, int amountOfPoints) {
        this.question = question;
        this.answers = answers;
        this.rightAnswer = rightAnswer;
        this.amountOfPoints = amountOfPoints;
    }
}
