package com.company;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.Random;

    public class Main {

        static Scanner scanner = new Scanner(System.in);
        static Random random = new Random();

        public static void main(String[] args) {

            ArrayList<Questions> questions = new ArrayList<>();
            System.out.println("Программа по составлению вопросов готова к работе!" +
                    "\n**************************************************");
            HelpCommand.helpCommand(questions);
        }

        public static void createBaseQue(ArrayList<Questions> questions) {
            System.out.println("Сколько вопросов Вы хотите ввести?");
            int amount_que = scanner.nextInt();
            for (int i = 0; i < amount_que; i++) {
                scanner.nextLine();
                System.out.println("Введите вопрос под номером " + (i + 1));
                String n = scanner.nextLine();
                ArrayList<String> allAns = new ArrayList<>();
                boolean isWork = true;
                System.out.println("Введите правильный вариант ответа >> ");
                String right_ans = scanner.nextLine();
                allAns.add(right_ans);
                String rightAns = right_ans;
                System.out.println("Введите варианты неправильных ответов. Если вы закончите их заполнение, напишите \"end\" >> ");
                while (isWork) {
                    String wrong_ans = scanner.nextLine();
                    if (!wrong_ans.equals("end")) allAns.add(wrong_ans);
                    else {
                        if (allAns.size() != 1) {
                            System.out.println("Хорошо. Заполнение ответов закончено");
                            isWork = false;
                        } else {
                            System.out.println("Недостаточно вариантов ответа");
                        }
                    }
                }
                System.out.println("Максимальный балл за этот вопрос: ");
                questions.add(new Questions(n, allAns, rightAns, scanner.nextInt()));
            }

            System.out.println("ВОПРОСЫ ГОТОВЫ!");
            HelpCommand.helpCommand(questions);
        }

        public static void startTesting(ArrayList<Questions> questions) {
            int wins = 0;
            System.out.println("Вы готовы начать тестирование? Начинаем!");
            System.out.println("Сколько вопросов Вы хотите протестировать? (attention: " + questions.size() + " - максимум)");
            int w = scanner.nextInt();
            long begin = System.currentTimeMillis();
            if (w > questions.size()) {
                System.out.println("Простите, но такого количества вопросов нет!");
                startTesting(questions);
            }
            int count = 0;
            ArrayList<String> test = new ArrayList<>();
            ArrayList<String> notRight = new ArrayList<>();
            while (count < w) {
                int a = random.nextInt(questions.size());
                boolean isContains = true;
                for (String t : test) {
                    if (questions.get(a).getQuestion().equals(t)) {
                        isContains = false;
                        break;
                    }
                }
                if (isContains) {
                    test.add(questions.get(a).getQuestion());
                    System.out.println(test.size() + ") " + test.get(test.size() - 1));
                    ArrayList<String> answers = new ArrayList<>();
                    while (answers.size() != questions.get(a).getAnswers().size()) {
                        int b = random.nextInt(questions.get(a).getAnswers().size());
                        boolean isContains1 = true;
                        for (String ans : answers) {
                            if (questions.get(a).getAnswers().get(b).equals(ans)) {
                                isContains1 = false;
                                break;
                            }
                        }
                        if (isContains1) answers.add(questions.get(a).getAnswers().get(b));
                    }
                    for (int i = 0; i < answers.size(); i++) {
                        System.out.println("  " + (i + 1) + ") " + answers.get(i));
                    }
                    int an = scanner.nextInt() - 1;
                    if (answers.get(an).equals(questions.get(a).getRightAnswer())) {
                        System.out.println("Правильно");
                        wins = wins + questions.get(a).getAmountOfPoints();
                    } else {
                        System.out.println("Неправильно");
                        notRight.add(questions.get(a).getQuestion());
                    }
                    count++;
                }
            }
            System.out.println(notRight.size() > (count / 2) ? "Тест пройден плохо!" : "Тест пройден!");
            System.out.println("Вы ответили на " + count + " вопрос(ов)");
            System.out.println("Ваш итоговый балл " + wins);
            System.out.println("Время прохождения теста " + (System.currentTimeMillis() - begin) + " мc.");
            if (notRight.size() != 0) {
                System.out.println("Вопросы, на которые были даны неправильные ответы:\n");
                for (int i = 0; i < notRight.size(); i++) {
                    System.out.println(notRight.get(i));
                    System.out.println("Правильный ответ - " + questions.get((questions.indexOf(notRight.get(i))) + 1).getRightAnswer() + "\n");
                }
            }
            HelpCommand.helpCommand(questions);
        }
    }

