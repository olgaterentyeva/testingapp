package com.company;

import java.util.ArrayList;
import java.util.Scanner;

public class HelpCommand {

    static Scanner scanner = new Scanner(System.in);

    public static void helpCommand(ArrayList<Questions> questions) {
        System.out.println("Что Вы хотите сделать? \n(1)Добавить вопросы в тест \n(2)Пройти тестирование \n(3)Выйти из программы");
        switch (scanner.nextInt()) {
            case 1:
                Main.createBaseQue(questions);
                break;
            case 2:
                if (questions.size() > 0) Main.startTesting(questions);
                else {
                    System.out.println("Количество вопросов в базе меньше запрашиваемого количества для тестирования!");
                }
                break;
            case 3:
                System.out.println("Программа закончила работу!");
                break;
            default:
                System.out.println("Неизвестная команда!");
                helpCommand(questions);
        }
    }
}
